# spotify-lua

Lua package for pulling data off of Spotify for a user, particularly their
current playing track.  This is most useful for a window manager, like Awesome
WM.

Might need to be a native Lua extension, so I can properly interface with
networking libraries without having to resort to lua dependencies and luarocks.
I might be able to just get away with luajit's ffi library.

Should not kill the WM if network is disconnected or on failure to work the
socket.
